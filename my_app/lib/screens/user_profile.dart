import 'package:flutter/material.dart';
import 'package:my_app/constants/routes.dart';
import 'package:my_app/services/cloud/cloud_users.dart';
import 'package:my_app/services/cloud/firebase_cloud_storage.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  CloudUser? _user;
  late final FirebaseCloudStorage _usersService;
  late final TextEditingController _name;
  late final TextEditingController _surname;
  late final TextEditingController _username;
  late final TextEditingController _number;
  late final TextEditingController _city;
  late final TextEditingController _description;

  @override
  void initState() {
    _usersService = FirebaseCloudStorage();
    _name = TextEditingController();
    _surname = TextEditingController();
    _username = TextEditingController();
    _number = TextEditingController();
    _city = TextEditingController();
    _description = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _name.dispose();
    _surname.dispose();
    _username.dispose();
    _number.dispose();
    _city.dispose();
    _description.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Edit Profile')),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(
              controller: _name,
              enableSuggestions: false,
              autocorrect: false,
              decoration: const InputDecoration(hintText: "Edit Name"),
            ),
            TextField(
              controller: _surname,
              enableSuggestions: false,
              autocorrect: false,
              decoration: const InputDecoration(hintText: "Edit Lastname"),
            ),
            TextField(
              controller: _username,
              enableSuggestions: false,
              autocorrect: false,
              decoration: const InputDecoration(hintText: "Edit Username"),
            ),
            TextField(
              controller: _number,
              enableSuggestions: false,
              autocorrect: false,
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(hintText: 'Edit Phone Number'),
            ),
            TextField(
              controller: _city,
              enableSuggestions: true,
              autocorrect: true,
              decoration: const InputDecoration(hintText: "Edit City"),
            ),
            TextField(
              controller: _city,
              enableSuggestions: true,
              autocorrect: true,
              maxLines: null,
              keyboardType: TextInputType.multiline,
              decoration: const InputDecoration(hintText: "Edit your bio"),
            ),
            OutlinedButton(
                onPressed: () async {
                  // final user = _user;
                  await _usersService.updateUser(
                    documentId: '',
                    name: _name.text,
                    lastname: _surname.text,
                    username: _username.text,
                    number: _number.text,
                    city: _city.text,
                    bio: _description.text,
                  );
                  // ignore: use_build_context_synchronously
                  Navigator.of(context).pushNamed(userRoute);
                },
                child: const Text('Save Changes'))
          ],
        ),
      ),
    );
  }
}
