import 'package:flutter/material.dart';
import 'package:my_app/constants/routes.dart';
import 'package:my_app/screens/user_profile.dart';
import 'package:my_app/services/cloud/cloud_users.dart';
import 'package:my_app/services/cloud/firebase_cloud_storage.dart';

class Users extends StatefulWidget {
  const Users({Key? key}) : super(key: key);

  @override
  State<Users> createState() => _UsersState();
}

class _UsersState extends State<Users> {
  late final FirebaseCloudStorage _usersService;

  @override
  void initState() {
    _usersService = FirebaseCloudStorage();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Users")),
      body: StreamBuilder(
        stream: _usersService.allUsers(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
            case ConnectionState.active:
              if (snapshot.hasData) {
                final allUsers = snapshot.data as Iterable<CloudUser>;
                return ListView.builder(
                  itemCount: allUsers.length,
                  itemBuilder: (context, index) {
                    final note = allUsers.elementAt(index);
                    return ListTile(
                      onTap: () {
                        Navigator.of(context).pushNamed(userProfileRoute);
                      },
                      title: Text(
                        note.name,
                        maxLines: 1,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                      ),
                      trailing: IconButton(
                          onPressed: () async {
                            await _usersService.delete(
                                documentId: note.documentId);
                          },
                          icon: const Icon(Icons.delete)),
                    );
                  },
                );
              } else {
                return const CircularProgressIndicator();
              }

            default:
              return const CircularProgressIndicator();
          }
        },
      ),
    );
  }
}
