import 'package:flutter/material.dart';
import 'package:my_app/constants/routes.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Dashboard')),
      body:Center(
        child: Column(
            children: [
              OutlinedButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed(createUserRoute);
                  },
                  child: const Text('Create user')),
              OutlinedButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed(userRoute);
                  },
                  child: const Text('Users')),
              
            ],
          ),
        ) ,
        floatingActionButton: FloatingActionButton(onPressed: () {
          Navigator.of(context).pushNamed(userProfileRoute);
        },
        tooltip: 'Edit Profile',
        child: const Icon(Icons.add),),
    );
  }
}
