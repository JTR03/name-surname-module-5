import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:my_app/services/cloud/cloud_storage_constants.dart';

class CloudUser {
  final String documentId;
  // final String userId;
  final String name;
  final String lastname;
  final String username;
  final String number;
  final String city;
  final String bio;

  CloudUser({
    required this.documentId,
    // required this.userId,
    required this.name,
    required this.lastname,
    required this.username,
    required this.number,
    required this.city,
    required this.bio,
  });

  CloudUser.fromSnapshot(QueryDocumentSnapshot<Map<String, dynamic>> snapshot)
      : documentId = snapshot.id,
        // userId = snapshot.data()[userIdField] as String,
        name = snapshot.data()[nameField] as String,
        lastname = snapshot.data()[lastnameField] as String,
        username = snapshot.data()[usernameField] as String,
        number = snapshot.data()[numberField],
        city = snapshot.data()[cityField] as String,
        bio = snapshot.data()[bioField] as String;
}
