import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_app/services/cloud/cloud_storage_constants.dart';
import 'package:my_app/services/cloud/cloud_users.dart';

class FirebaseCloudStorage {
  final users = FirebaseFirestore.instance.collection('user');

  Future<void> delete({required String documentId}) async {
    try {
      await users.doc(documentId).delete();
    } catch (e) {
      const Text('Cannot delete note');
    }
  }

  Future<void> updateUser({
    required String documentId,
    required String name,
    required String lastname,
    required String username,
    required String number,
    required String city,
    required String bio,
  }) async {
    try {
      await users.doc(documentId).update({
        nameField: name,
        lastnameField: lastname,
        usernameField: username,
        numberField: number,
        cityField: city,
        bioField: bio,
      });
    } catch (e) {
      const Text('Could not update');
    }
  }

  Stream<Iterable<CloudUser>> allUsers() => users.snapshots().map(
        (event) => event.docs.map((doc) => CloudUser.fromSnapshot(doc)),
      );

  Future<Iterable<CloudUser>> getUser({required String userId}) async {
    return await users.where(userIdField, isEqualTo: userId).get().then(
          (value) => value.docs.map(
            (doc) => CloudUser.fromSnapshot(doc),
          ),
        );
  }

  Future<CloudUser> createNewUser({
    required String name,
    required String lastname,
    required String username,
    required String number,
    required String city,
    required String bio,
  }) async {
    final document = await users.add({
      // userIdField: userId,
      nameField: name,
      lastnameField: lastname,
      usernameField: username,
      numberField: number,
      cityField: city,
      bioField: bio,
    });
    final fetchedUser = await document.get();
    return CloudUser(
      documentId: fetchedUser.id,
      name: '',
      lastname: '',
      username: '',
      number: '',
      city: '',
      bio: '',
      // userId: userId,
    );
  }

  static final FirebaseCloudStorage _shared =
      FirebaseCloudStorage._sharedInstance();
  FirebaseCloudStorage._sharedInstance();
  factory FirebaseCloudStorage() => _shared;
}
